<?php
/**
 * Created by PhpStorm.
 * User: Oli
 * Date: 01.10.2018
 * Time: 09:25
 */
require_once __DIR__ . '/./static/Shopware_API_V2_client.class.php';

class documentCopyApi
{
    private $client;
    private $baseName;
    private $basePath;
    private $copyPath;
    private $orderNumber;

    public function __construct() {
        $this->apiData = json_decode(file_get_contents( __DIR__ .'/static/config/api.json' ), true);
        $url = $this->apiData['url'];
        $user = $this->apiData['user'];
        $key = $this->apiData['key'];
        $this->client = new ApiClient(
                   $url, $user, $key
               );
        $this->baseName=$this->apiData['baseName'];
        $this->basePath=$this->apiData['basePath'];
        $this->copyPath=$this->apiData['copyPath'];

    }

    public function copyPdf($orderId){
        $order=$this->getOrder($orderId);
        $documents=$this->getOrderDocuments($order);
        $this->orderNumber=$order['data']['number'];
        $invoiceDocuments=$this->getInvoiceDocuments($documents);
        $invoiceDocumentIds=$this->getInvoiceDocumentIds($invoiceDocuments);
        $this->getInvoicePdf($invoiceDocumentIds);
    }

    public function getInvoiceDocumentIds($invoiceDocuments){
        $documentIds=[];
        foreach($invoiceDocuments as $invoiceDocument){
            $documentIds[]=$invoiceDocument['id'];
        }
        return $documentIds;
    }

    public function getInvoicePdf($invoiceDocumentIds){
        $pdfDocuments=[];
        foreach($invoiceDocumentIds as $invoiceDocumentId){

            $document= $this->client->get('documents', [$invoiceDocumentId]);
             $this->createInvoiceDocuments($document['data']['pdfDocument']);
        }
        return $pdfDocuments;
    }

    public function createInvoiceDocuments($pdfDocument){
        $rute= __DIR__ .'/'.$this->basePath.$this->baseName.$this->orderNumber.'.pdf';
        $rute2= __DIR__ .'/'.$this->copyPath.$this->baseName.$this->orderNumber.'.pdf';
        $b64Doc = base64_decode($pdfDocument);
        file_put_contents($rute, $b64Doc);
        file_put_contents($rute2, $b64Doc);
    }

    public function getInvoiceDocuments($documents){
        $invoiceDocuments=[];
        if(count($documents)>0){
        foreach($documents as $document){
            if($document['typeId']==1){
                $invoiceDocuments[]=$document;
            }
        }
        }
        return $invoiceDocuments;
    }

    public function getOrder($orderId){
        return $this->client->get('orders', [$orderId]);
    }

    public function getOrderId($orderNumber){
        $order= $this->client->get('orders', [$orderNumber.'?useNumberAsId=true']);
        return $order['data']['id'];
    }

    public function getOrderDocuments($order){
        return $order['data']['documents'];
    }
}



$orderId='';
$API = new documentCopyApi();

$all_args = $argv[1];
$get_strings = explode("&", $all_args);
foreach ($get_strings as $get_string) {
    $pair = explode('=', $get_string);
    $_GET[$pair[0]] = $pair[1];
}

//$_GET['orderNumber']=45011372;

if(isset($_GET['orderId'])){
    $orderId=$_GET['orderId'];
}elseif(isset($_GET['orderNumber'])){

    $orderId=$API->getOrderId($_GET['orderNumber']);
}
echo "orderId ".$orderId;
$API->copyPdf($orderId);