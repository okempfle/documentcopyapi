<?php

class ApiClient
{
    const METHOD_GET = 'GET';
    const METHOD_PUT = 'PUT';
    const METHOD_POST = 'POST';
    const METHOD_DELETE = 'DELETE';
    protected $validMethods = [
        self::METHOD_GET,
        self::METHOD_PUT,
        self::METHOD_POST,
        self::METHOD_DELETE,
    ];
    protected $apiUrl;
    protected $cURL;

    public static function fromConfig($configFile) {
       if (!file_exists($configFile)) {
           throw new RuntimeException("File not found: " . $configFile);
       }
       $config = json_decode(file_get_contents($configFile), true);
       if (json_last_error() !== JSON_ERROR_NONE) {
           throw new RuntimeException($configFile . ": " . json_last_error_msg());
       }
       return new self($config["api_url"], $config["api_user"], $config["api_key"]);
    }

    public function __construct($apiUrl, $username, $apiKey)
    {
        $this->apiUrl = rtrim($apiUrl, '/') . '/';
        //Initializes the cURL instance
        $this->cURL = curl_init();
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->cURL, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($this->cURL, CURLOPT_USERAGENT, 'Shopware ApiClient');
        curl_setopt($this->cURL, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($this->cURL, CURLOPT_USERPWD, $username . ':' . $apiKey);
        curl_setopt(
            $this->cURL,
            CURLOPT_HTTPHEADER,
            ['Content-Type: application/json; charset=utf-8']
        );
    }

    public function call($url, $method = self::METHOD_GET, $data = [], $params = [])
    {
        if (!in_array($method, $this->validMethods)) {
            throw new Exception('Invalid HTTP-Methode: ' . $method);
        }
        $queryString = '';
        if (!empty($params)) {
            $queryString = http_build_query($params);
        }
        if($method=='GET'){

            $url = $this->apiUrl . $url .'/'. $params[0];
        }else{
            $url = rtrim($url, '?') . '?';
            $url = $this->apiUrl . $url . $queryString;
        }
        $dataString = json_encode($data);
        curl_setopt($this->cURL, CURLOPT_URL, $url);
        curl_setopt($this->cURL, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($this->cURL, CURLOPT_POSTFIELDS, $dataString);
        $result = curl_exec($this->cURL);
        $httpCode = curl_getinfo($this->cURL, CURLINFO_HTTP_CODE);

        return $this->prepareResponse($result, $httpCode);
    }

    public function get($url, $params = [])
    {
        return $this->call($url, self::METHOD_GET, [], $params);
    }

    public function post($url, $data = [], $params = [])
    {
        return $this->call($url, self::METHOD_POST, $data, $params);
    }

    public function put($url, $data = [], $params = [])
    {
        return $this->call($url, self::METHOD_PUT, $data, $params);
    }

    public function delete($url, $params = [])
    {
        return $this->call($url, self::METHOD_DELETE, [], $params);
    }

    protected function prepareResponse($result, $httpCode)
    {
        // echo "<h2>HTTP: $httpCode</h2>";
        if (null === $decodedResult = json_decode($result, true)) {
            $jsonErrors = [
                JSON_ERROR_NONE => 'No error occurred',
                JSON_ERROR_DEPTH => 'The maximum stack depth has been reached',
                JSON_ERROR_CTRL_CHAR => 'Control character issue, maybe wrong encoded',
                JSON_ERROR_SYNTAX => 'Syntaxerror',
            ];
            // echo '<h2>Could not decode json</h2>';
            // echo 'json_last_error: ' . $jsonErrors[json_last_error()];
            // echo '<br>Raw:<br>';
            // echo '<pre>' . print_r($result, true) . '</pre>';

            return [
                'success' => false,
                'data' => [
                    'message' => 'Could not decode json!',
                    'error' => $jsonErrors[json_last_error()],
                    'result' => $result
                ]
            ];
        }
        if (!isset($decodedResult['success'])) {
            // echo 'Invalid Response';

            return [
                'success' => false,
                'data' => [
                    'message' => 'Invalid Response!',
                    'result' => $result
                ]
            ];
        }
        if (!$decodedResult['success']) {
            echo '<h2>No Success</h2>';
            echo '<p>' . $decodedResult['message'] . '</p>';
            if (array_key_exists('errors', $decodedResult) && is_array($decodedResult['errors'])) {
                echo '<p>' . join('</p><p>', $decodedResult['errors']) . '</p>';
            }

            return [
                'success' => false,
                'data' => [
                    'message' => 'No Success!',
                    'error' => $decodedResult['errors'],
                    'result' => $decodedResult['message']
                ]
            ];
        }
        // echo '<h2>Success</h2>';
        // if (isset($decodedResult['data'])) {
            // echo '<pre>' . print_r($decodedResult['data'], true) . '</pre>';
        // }

        return $decodedResult;
    }
}
